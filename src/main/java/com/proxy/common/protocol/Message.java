package com.proxy.common.protocol;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;

/**
 * 用于 用户和代理服务之间的通信
 * 不用于 代理服务器和客户端之间的通信
 */
public class Message {


    /**
     * 服务器地址
     */
    private String serverAddress;

    /**
     * 用户通道
     */
    private Channel userChannel;


    /**
     * 服务器端口
     */
    private Integer sPort;

    /**
     * 会话id
     */
    private Long sessionID;

    /**
     * 真实服务器地址
     */
    private String remoteAddress;


    /**
     * 真实服务器端口
     */
    private int remotePort;

    /**
     * 消息类型
     */
    private byte type;

    /**
     * 代理类型
     */
    private int proxyType;

    /**
     * 优先级
     */
    private byte priority;

    /**
     * 数据
     */
    private byte[] data;

    public Integer getsPort() {
        return sPort;
    }

    public void setsPort(Integer sPort) {
        this.sPort = sPort;
    }

    public Long getSessionID() {
        return sessionID;
    }

    public void setSessionID(Long sessionID) {
        this.sessionID = sessionID;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public void setProxyType(byte proxyType) {
        this.proxyType = proxyType;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public int getProxyType() {
        return proxyType;
    }

    public void setProxyType(int proxyType) {
        this.proxyType = proxyType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Channel getUserChannel() {
        return userChannel;
    }

    public void setUserChannel(Channel userChannel) {
        this.userChannel = userChannel;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }
}
