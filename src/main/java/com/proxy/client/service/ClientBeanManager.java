package com.proxy.client.service;


/**
 * 客户端 实例化服务
 */
public class ClientBeanManager {


    private static ProxyService proxyService=new ProxyService();

    public static ProxyService getProxyService() {
        return proxyService;
    }
}
