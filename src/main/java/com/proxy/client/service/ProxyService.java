package com.proxy.client.service;


import com.proxy.client.dao.ProxyDao;
import io.netty.channel.Channel;

public class ProxyService {

    private static ProxyDao proxyDao=new ProxyDao();

    public  Long getRealServerChannelSessionID(Channel realServerChannel) {
        return proxyDao.getRealServerChannelSessionID(realServerChannel);
    }

    public  Channel getRealServerChannel(Long sessionID) {
        return proxyDao.getRealServerChannel(sessionID);
    }

    public  void addRealServerChannel(Long sessionID, Channel realServerChannel,int proxyType,String proxyServerName,String realServer) {
        proxyDao.addRealServerChannel(sessionID,realServerChannel,proxyType,proxyServerName,realServer);
    }
    public  int getProxyType(Channel realServerChannel) {
        return proxyDao.getProxyType(realServerChannel);
    }
    public  String getProxyServer(Channel realServerChannel) {
        return proxyDao.getProxyServerName(realServerChannel);
    }

    public String getRealServerName(Channel realServerChannel) {
        return proxyDao.getRealServerName(realServerChannel);
    }

    public void removeRealServerChannel(Long sessionID){
        proxyDao.removeRealServerChannel(sessionID);

    }
    public void clear(){
        proxyDao.clear();
    }
}
