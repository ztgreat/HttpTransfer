package com.proxy.client.handler;

import com.proxy.client.service.ClientBeanManager;
import com.proxy.common.protocol.CommonConstant;
import com.proxy.server.service.ServerBeanManager;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RemoteTCPHandler extends ChannelInboundHandlerAdapter {

    private static Logger logger = LoggerFactory.getLogger(RemoteTCPHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        Channel realServerChannel = ctx.channel();

        int proxyType=ClientBeanManager.getProxyService().getProxyType(realServerChannel);
        Long sessionID= ClientBeanManager.getProxyService().getRealServerChannelSessionID(realServerChannel);

        Channel usercChannel = ServerBeanManager.getUserSessionService().get(sessionID);

        if (usercChannel == null) {
            // 代理客户端连接断开
            ReferenceCountUtil.release(msg);
            ctx.channel().close();
            logger.debug("客户端和代理服务器失去连接");
        } else {

            //http 消息
            if(proxyType == CommonConstant.ProxyType.HTTP){
                //向上传递
                ctx.fireChannelRead(msg);
            }else if(proxyType == CommonConstant.ProxyType.HTTPS) {

                //向上传递
                ctx.fireChannelRead(msg);

            }else if(proxyType == CommonConstant.ProxyType.TCP){
                logger.debug("收到真实服务器TCP类型消息");
                usercChannel.writeAndFlush(msg);
            }
            else {
                ReferenceCountUtil.release(msg);
                logger.error("代理类型错误");
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("异常:与真实服务器连接断开:"+cause.getMessage());
        removeConnect(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.debug("与真实服务器连接断开");
        //removeConnect(ctx);
    }
    public  void removeConnect(ChannelHandlerContext ctx){
        Long sessionID= ClientBeanManager.getProxyService().getRealServerChannelSessionID(ctx.channel());
        ClientBeanManager.getProxyService().removeRealServerChannel(sessionID);
    }
}
