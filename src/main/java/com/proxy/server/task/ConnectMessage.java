package com.proxy.server.task;


import com.proxy.client.service.ClientBeanManager;
import com.proxy.common.protocol.CommonConstant;
import com.proxy.common.protocol.Message;
import com.proxy.server.ProxyServer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 建立连接消息
 */
public class ConnectMessage implements Runnable {



    private static Logger logger = LoggerFactory.getLogger(ConnectMessage.class);

    private  Message message;

    public ConnectMessage(Message message) {
        this.message = message;
    }

    @Override
    public void run() {

        String ip = message.getRemoteAddress();

        int port = message.getRemotePort();

        int proxyType = message.getProxyType();

        Bootstrap bootstrap;

        if (proxyType == CommonConstant.ProxyType.HTTPS){
            bootstrap = ProxyServer.getHttpsRealServerBootstrap();
        }else if(proxyType == CommonConstant.ProxyType.HTTP){
            bootstrap = ProxyServer.getHttpRealServerBootstrap();
        }else if(proxyType == CommonConstant.ProxyType.TCP){
            bootstrap = ProxyServer.getHttpRealServerBootstrap();
        }else {
            logger.error("代理类型错误");
            return;
        }

        bootstrap.connect(ip, port).addListener(new ChannelFutureListener() {

            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {

                    Channel realServerChannel = future.channel();

                    logger.debug("连接真实服务器成功{}",ip+":"+port);

                    ClientBeanManager.getProxyService().addRealServerChannel(message.getSessionID(),realServerChannel,message.getProxyType(),message.getServerAddress(),ip);
                    message.getUserChannel().config().setAutoRead(true);
                    logger.debug("用户通道设置为可读");
                } else {
                    logger.error("客户端连接真实服务器({})失败:{}",ip+":"+port+" "+future.cause().getMessage());
                    message.getUserChannel().close();
                }
            }
        });


    }
}
