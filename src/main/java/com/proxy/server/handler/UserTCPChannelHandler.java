package com.proxy.server.handler;


import com.proxy.client.service.ClientBeanManager;
import com.proxy.common.entity.ProxyRealServer;
import com.proxy.common.protocol.CommonConstant;
import com.proxy.common.protocol.Message;
import com.proxy.server.service.ServerBeanManager;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

/**
 * 处理用户请求的handler
 * @author ztgreat
 */
public class UserTCPChannelHandler extends ChannelInboundHandlerAdapter {

    private static Logger logger = LoggerFactory.getLogger(UserTCPChannelHandler.class);

    public UserTCPChannelHandler(){
        super();
    }

    /**
     * 为用户连接产生ID
     * @return
     */
    private static Long getSessionID() {
        return ServerBeanManager.getSessionIDGenerate().generateId();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {


        Channel userChannel = ctx.channel();

        System.out.println(userChannel.toString());
        InetSocketAddress sa = (InetSocketAddress) userChannel.localAddress();

        ProxyRealServer realServer = ServerBeanManager.getProxyChannelService().getServerProxy(sa.getPort());

        if (realServer == null) {
            logger.error("端口{} 没有代理客户端",sa.getPort());
            ctx.channel().close();
            return;
        }
        long sessionID=getSessionID();

        //将sessionID，用户ip,port，服务器port，封装到消息中

        Message message=new Message();
        message.setSessionID(sessionID);
        message.setType(CommonConstant.MessageType.TYPE_CONNECT_REALSERVER);
        message.setRemoteAddress(realServer.getRealHost());
        message.setRemotePort(realServer.getRealHostPort());
        message.setProxyType(realServer.getProxyType().byteValue());
        message.setServerAddress(sa.getHostString()+":"+sa.getPort());
        message.setUserChannel(userChannel);

        //将通道保存 调用UserService(会保存代理类型 type)
        ServerBeanManager.getUserSessionService().add(sessionID,ctx.channel(),realServer.getProxyType());

        //调用ServerService 将消息 放入队列
        ServerBeanManager.getTransferService().toClient(message);
        logger.debug("通知与真实服务器{}建立连接 ",realServer.getFullAddress());

        ctx.channel().config().setAutoRead(false);
        logger.debug("用户请求访问通道设置为不可读");

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        //代理类型
        Integer type= ServerBeanManager.getUserSessionService().getType(ctx.channel());
        if (type!=null){
            if(type.intValue()== CommonConstant.ProxyType.HTTP){
                //http 类型
                ctx.fireChannelRead(msg);
                logger.debug("http代理");
            }else if(type.intValue()== CommonConstant.ProxyType.HTTPS){

                ctx.fireChannelRead(msg);
                logger.debug("https代理");

            }else if(type.intValue()== CommonConstant.ProxyType.TCP){
                tcpHandler(ctx,(ByteBuf) msg);
            }
            else {
                ReferenceCountUtil.release(msg);
                logger.error("代理类型错误:丢弃消息");
            }

        }else {
            ReferenceCountUtil.release(msg);
            logger.error("消息格式错误:丢弃消息");
        }

    }


    /**
     * 处理tcp 请求
     * @param ctx
     * @param buf
     * @throws Exception
     */
    public  void tcpHandler(ChannelHandlerContext ctx, ByteBuf buf) throws  Exception{


        Channel userChannel=ctx.channel();

        InetSocketAddress sa = (InetSocketAddress) userChannel.localAddress();

        //获取代理信息
        ProxyRealServer realServer = ServerBeanManager.getProxyChannelService().getServerProxy(sa.getPort());

        if (realServer == null) {
            ctx.channel().close();
            ReferenceCountUtil.release(buf);
            logger.error("{}: 没有代理客户端",sa.getPort());
            return;
        }

        //封装消息
        Long sessionID= ServerBeanManager.getUserSessionService().getSessionID(userChannel);

        Channel channel= ClientBeanManager.getProxyService().getRealServerChannel(sessionID);
        channel.writeAndFlush(buf);

    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {

        logger.debug("用户连接失效");
        Long sessionID= ServerBeanManager.getUserSessionService().getSessionID(ctx.channel());

        if (sessionID==null){
            return;
        }
        ServerBeanManager.getUserSessionService().remove(sessionID);
        closeChannle(ctx);
        ClientBeanManager.getProxyService().getRealServerChannel(sessionID).close();
    }


    /**
     * 关闭用户连接
     * @param ctx
     */
    public void   closeChannle(ChannelHandlerContext ctx){

        Channel channel=ctx.channel();

        if (channel!=null && channel.isActive()){
            channel.close();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("发生异常({})", cause.getMessage());
        closeChannle(ctx);
    }
}
