package com.proxy.server.handler;


import com.proxy.client.service.ClientBeanManager;
import com.proxy.common.codec.http.MyHttpRequestEncoder;
import com.proxy.common.entity.ProxyRealServer;
import com.proxy.common.protocol.CommonConstant;
import com.proxy.common.util.IPGenerate;
import com.proxy.server.service.ServerBeanManager;
import com.proxy.server.util.ProxyUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.util.ReferenceCountUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ztgreat
 */
public class HttpChannelHandler extends ChannelInboundHandlerAdapter{

    private static Logger logger = LoggerFactory.getLogger(HttpChannelHandler.class);

    /**
     * 用于http 消息编码处理
     * @// TODO: 2018/2/10 需要review
     */
    private MyHttpRequestEncoder httpRequestEncoder;


    public HttpChannelHandler(){
        super();
        httpRequestEncoder =new MyHttpRequestEncoder();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        //http 请求
        if (msg instanceof FullHttpRequest){
            FullHttpRequest request = (FullHttpRequest) msg;

            //处理http消息
            httpHandler(ctx,request);
        }else {
            ReferenceCountUtil.release(msg);
            logger.error("丢弃消息");
        }
    }

    /**
     * 处理http 请求
     * @param ctx
     * @param request
     * @throws Exception
     */
    public  void httpHandler(ChannelHandlerContext ctx,FullHttpRequest request) throws  Exception{


        Channel userChannel=ctx.channel();

        InetSocketAddress sa = (InetSocketAddress) userChannel.localAddress();

        //获取代理信息
        ProxyRealServer realServer = ServerBeanManager.getProxyChannelService().getServerProxy(sa.getPort());

        //如果不存在domain,那么不存在该代理客户端，后续理应不再判断，暂时为了避免考虑不全，先判断多次
        if (realServer == null) {
            ctx.channel().close();
            ReferenceCountUtil.release(request);
            logger.error("{}: 没有代理客户端",sa.getPort());
            return;
        }


        //封装消息
        Long sessionID= ServerBeanManager.getUserSessionService().getSessionID(userChannel);


        String oldHost= request.headers().get(HttpHeaderNames.HOST);

        /**
         *TODO 需要修改 Referer 属性
         */
        String referer=request.headers().get(HttpHeaderNames.REFERER);
        if(StringUtils.isNotBlank(referer)){
            if(StringUtils.isNotBlank(realServer.getFullAddress())) {

                String newReferer;
                if(realServer.getRealHostPort().intValue() == CommonConstant.DEFAULT_HTTP_PORT){
                    newReferer = referer.replace(oldHost,realServer.getRealHost());
                }else {
                    newReferer=referer.replace(oldHost,realServer.getFullAddress());
                }
                request.headers().set(HttpHeaderNames.REFERER,newReferer);
            }
        }

        //设置host为请求服务器地址
        if(realServer.getRealHostPort().intValue() == CommonConstant.DEFAULT_HTTP_PORT){
            request.headers().set(HttpHeaderNames.HOST,realServer.getRealHost());
        }else {
            request.headers().set(HttpHeaderNames.HOST,realServer.getFullAddress());
        }


        String forward = realServer.getForward();

        if(StringUtils.isNotBlank(forward)){

            if(CommonConstant.HeaderAttr.Forwarded_Random.equals(forward)){
                request.headers().set("X-Forwarded-For", IPGenerate.getRandomIp());
            }else if(CommonConstant.HeaderAttr.Forwarded_Default.equals(forward)){
                request.headers().add("X-Forwarded-For", sa.getHostString());
            }else if(ProxyUtil.isIpAddr(forward)){
                request.headers().set("X-Forwarded-For", forward);
            }
        }

        //设置 sessionID,记录会话
        request.headers().add(CommonConstant.SESSION_NAME,sessionID);

        List<Object>list=new ArrayList<>();
        httpRequestEncoder.encode(ctx,request,list);

        Channel channel= ClientBeanManager.getProxyService().getRealServerChannel(sessionID);
        for (Object o:list){
            ByteBuf buf= (ByteBuf)o;
            channel.writeAndFlush(buf);
        }
        ReferenceCountUtil.release(request);

    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("用户请求发生异常");
        ctx.channel().close();
    }
}
