package com.proxy.server.event;


/**
 * 暂时没用
 */
public class TransferEvent {

   private boolean transfer;

    public TransferEvent() {
        this.transfer = true;
    }

    public boolean isTransfer() {
        return transfer;
    }

    public void setTransfer(boolean transfer) {
        this.transfer = transfer;
    }
}
