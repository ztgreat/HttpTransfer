package com.proxy.server.dao;

import com.proxy.common.cache.Cache;
import com.proxy.common.cache.CacheManager;
import com.proxy.common.cache.memory.MemoryCacheManager;
import com.proxy.common.entity.ProxyRealServer;
import com.proxy.server.handler.HttpChannelHandler;
import com.proxy.server.handler.UserTCPChannelHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.ssl.SslHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

/**
 * 本地 端口(tcp)/域名 绑定 具体操作层
 * @author ztgreat
 */
public class ProxyChannelDao {

    private static Logger logger = LoggerFactory.getLogger(ProxyChannelDao.class);

    private static CacheManager<Object,ProxyRealServer> cacheManager =new MemoryCacheManager<Object,ProxyRealServer>();

    /**
     * 服务器端口/访问域名 -- 代理通道 映射
     */
    private static Cache<Object,ProxyRealServer> proxyChannelCache = cacheManager.getCache("proxy_cache");


    /**
     * 解绑 服务器端口
     * @param serverPort 需要解绑的端口
     * @return
     */
    public  boolean unBind(Integer serverPort){
        if (serverPort==null)
            return false;
        ProxyRealServer realServer= proxyChannelCache.get(serverPort);
        if (realServer==null)
            return false;
        realServer.getChannel().close();
        //不移除
//        proxyChannelCache.remove(serverPort);
        return  true;


    }

    public ProxyRealServer getServerProxy(Object key) {
        return proxyChannelCache.get(key);
    }

    public ChannelFuture bindHTTP(ProxyRealServer realServer) {


        NioEventLoopGroup serverWorkerGroup=new NioEventLoopGroup();
        NioEventLoopGroup serverBossGroup=new NioEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap();

        bootstrap.group(serverBossGroup, serverWorkerGroup).channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {

                        ch.pipeline().addLast(new UserTCPChannelHandler());
                        //http请求消息解码器
                        ch.pipeline().addLast("httpDecoder",new HttpRequestDecoder());
                        //解析 HTTP POST 请求
                        ch.pipeline().addLast("httpObject",new HttpObjectAggregator(2*1024*1024));
                        ch.pipeline().addLast("transferHandler",new HttpChannelHandler());
                    }
                });



        return bootstrap.bind(realServer.getServerPort()).addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture channelFuture) throws Exception {

                if (channelFuture.isSuccess()){
                    logger.info("绑定本地服务端口({})成功",realServer.getServerPort());
                    //绑定成功

                    realServer.setChannel(channelFuture.channel());

                    proxyChannelCache.put(realServer.getServerPort(),realServer);


                    channelFuture.channel().closeFuture().addListeners(new ChannelFutureListener() {
                        @Override
                        public void operationComplete(ChannelFuture channelFuture) throws Exception {
                            logger.info("等待代理服务退出...");
                            serverWorkerGroup.shutdownGracefully();
                            serverBossGroup.shutdownGracefully();
                        }
                    });


                }
                else {
                    logger.error("绑定本地服务端口{}失败:{}",realServer.getServerPort(),channelFuture.toString());
                }

            }
        });

        
    }

    public ChannelFuture bindHTTPS(ProxyRealServer realServer, SSLContext sslContext){
        NioEventLoopGroup serverWorkerGroup=new NioEventLoopGroup();
        NioEventLoopGroup serverBossGroup=new NioEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap();

        bootstrap.group(serverBossGroup, serverWorkerGroup).channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {

                        if(sslContext != null){
                            SSLEngine sslEngine = sslContext.createSSLEngine();
                            sslEngine.setUseClientMode(false); //服务器端模式
                            sslEngine.setNeedClientAuth(false); //不需要验证客户端
                            ch.pipeline().addLast("ssl", new SslHandler(sslEngine));
                        }
                        ch.pipeline().addLast(new UserTCPChannelHandler());
                        //http请求消息解码器
                        ch.pipeline().addLast("httpDecoder",new HttpRequestDecoder());
                        //解析 HTTP POST 请求
                        ch.pipeline().addLast("httpObject",new HttpObjectAggregator(2*1024*1024));
                        ch.pipeline().addLast("transferHandler",new HttpChannelHandler());
                    }
                });

        return bootstrap.bind(realServer.getServerPort()).addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture channelFuture) throws Exception {

                if (channelFuture.isSuccess()){
                    logger.info("绑定本地服务端口({})成功",realServer.getServerPort());
                    realServer.setChannel(channelFuture.channel());
                    proxyChannelCache.put(realServer.getServerPort(),realServer);
                    channelFuture.channel().closeFuture().addListeners(new ChannelFutureListener() {
                        @Override
                        public void operationComplete(ChannelFuture channelFuture) throws Exception {
                            logger.info("等待代理服务退出...");
                            serverWorkerGroup.shutdownGracefully();
                            serverBossGroup.shutdownGracefully();
                        }
                    });
                }
                else {
                    logger.error("绑定本地服务端口{}失败:{}",realServer.getServerPort(),channelFuture.toString());
                }

            }
        });
    }
}
