package com.proxy.server.service;

import com.proxy.common.entity.ProxyRealServer;
import com.proxy.server.dao.ProxyChannelDao;
import io.netty.channel.ChannelFuture;

import javax.net.ssl.SSLContext;

/**
 * 本地 端口(tcp)/域名 绑定服务层
 * @author  ztgreat
 */
public class ProxyChannelService {


    private static ProxyChannelDao proxyChannelDao =new ProxyChannelDao();

    /**
     * 绑定http服务
     * @throws InterruptedException
     */
    public ChannelFuture bindHTTPService(ProxyRealServer realServer) throws  Exception{
        return proxyChannelDao.bindHTTP(realServer);
    }

    /**
     * 绑定https 服务
     * @param realServer
     * @return
     * @throws Exception
     */
    public ChannelFuture bindHTTPSService(ProxyRealServer realServer, SSLContext sslContext) throws  Exception{
        return proxyChannelDao.bindHTTPS(realServer,sslContext);
    }


    /**
     * 解绑 服务器端口
     * @param serverPort 需要解绑的端口
     * @return
     */
    public  boolean unBind(Integer serverPort){

       return proxyChannelDao.unBind(serverPort);
    }


    /**
     * 获取代理信息
     * @param key 服务器端口
     * @return
     */
    public ProxyRealServer getServerProxy(Object key){
        return  proxyChannelDao.getServerProxy(key);
    }


}
