package com.proxy.server;

import com.proxy.client.handler.HttpSendHandler;
import com.proxy.client.handler.RemoteTCPHandler;
import com.proxy.common.entity.ProxyRealServer;
import com.proxy.common.protocol.CommonConstant;
import com.proxy.common.ssl.SecureChatSslContextFactory;
import com.proxy.server.service.LogBackConfigLoader;
import com.proxy.server.service.ServerBeanManager;
import com.proxy.server.util.ProxyUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpResponseDecoder;
import io.netty.handler.ssl.SslHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.List;
import java.util.Map;

public class ProxyServer {


    private static Logger logger = LoggerFactory.getLogger(ProxyServer.class);
    /**
     * 2M
     */
    private int maxContentLength = 2*1024 * 1024;

    /**
     * 连接真实服务器http启动器
     */
    private static Bootstrap httpRealServerBootstrap;

    /**
     * 用于真实服务器
     */
    private NioEventLoopGroup httpRealServerGroup;



    /**
     * 连接真实服务器https启动器
     */
    private static Bootstrap httpsRealServerBootstrap;

    /**
     * 用于真实服务器
     */
    private NioEventLoopGroup httpsRealServerGroup;


    /**
     * 是否启用https服务
     */
    private boolean isSSL;


    public ProxyServer() {
    }

    public static Bootstrap getHttpRealServerBootstrap() {
        return httpRealServerBootstrap;
    }

    public static Bootstrap getHttpsRealServerBootstrap() {
        return httpsRealServerBootstrap;
    }


    public  void start() throws  Exception{

        //读取代理服务配置文件
        ServerBeanManager.getConfigService().readServerConfig();

        try {
            //配置代理信息
            configurProxy();
            ServerBeanManager.getTransferService().start();
            initRealServerBoot();
        }catch (Exception e){
            return;
        }

    }


    /**
     * 配置代理信息
     * @throws Exception
     */
    public void configurProxy() throws Exception{

        List<Map<String,Object>> nodes = (List<Map<String,Object>>) ServerBeanManager.getConfigService().getConfigure("transfer");

        for (Map<String,Object>node :nodes){

            ProxyRealServer proxy=new ProxyRealServer();
            proxy.setRealHost((String) node.get("realhost"));
            proxy.setRealHostPort((Integer) node.get("realhostport"));
            proxy.setDescription((String) node.get("description"));
            String proxyType= (String) node.get("proxyType");
            Integer serverport = (Integer) node.get("serverport");
            String forward = (String) node.get("forward");
            if(forward != null){
                if(CommonConstant.HeaderAttr.Forwarded_Default.equals(forward)){
                    //指定为服务器ip
                    proxy.setForward(CommonConstant.HeaderAttr.Forwarded_Default);
                }else if(CommonConstant.HeaderAttr.Forwarded_Random.equals(forward)){
                    //随机ip
                    proxy.setForward(CommonConstant.HeaderAttr.Forwarded_Random);
                }else if(ProxyUtil.isIpAddr(forward)){
                    //用户指定ip
                    proxy.setForward(forward);
                }else if(!CommonConstant.HeaderAttr.Forwarded_None.equals(forward)){
                    logger.error("配置文件出错,代理forward 配置错误");
                    throw  new RuntimeException();
                }
            }
            if (proxyType.equalsIgnoreCase("http")){
                proxy.setServerPort(serverport);
                proxy.setProxyType(CommonConstant.ProxyType.HTTP);
                ServerBeanManager.getProxyChannelService().bindHTTPService(proxy);

            }else if(proxyType.equalsIgnoreCase("https")){

                isSSL = true;
                proxy.setServerPort(serverport);
                proxy.setProxyType(CommonConstant.ProxyType.HTTPS);
                ServerBeanManager.getProxyChannelService().bindHTTPSService(proxy,null);

            }else if(proxyType.equalsIgnoreCase("tcp")){
                proxy.setServerPort(serverport);
                proxy.setProxyType(CommonConstant.ProxyType.TCP);
                ServerBeanManager.getProxyChannelService().bindHTTPService(proxy);
            }else {
                continue;
            }

        }
    }

    /**
     * 初始化 连接后端真正服务器
     */
    public  void initRealServerBoot(){

        //初始化 http服务
        httpRealServerBootstrap = new Bootstrap();
        httpRealServerGroup = new NioEventLoopGroup();
        httpRealServerBootstrap.group(httpRealServerGroup);
        httpRealServerBootstrap.channel(NioSocketChannel.class);
        httpRealServerBootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline().addLast(new RemoteTCPHandler());
                ch.pipeline().addLast(new HttpResponseDecoder());
                ch.pipeline().addLast(new HttpObjectAggregator(maxContentLength));
                ch.pipeline().addLast(new HttpSendHandler());
            }
        });

        if(isSSL){
            //初始化 https服务
            httpsRealServerBootstrap = new Bootstrap();
            httpsRealServerGroup = new NioEventLoopGroup();
            httpsRealServerBootstrap.group(httpsRealServerGroup);
            httpsRealServerBootstrap.channel(NioSocketChannel.class);
            httpsRealServerBootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {

                    SSLContext context=SecureChatSslContextFactory.getClientContext();
                    SSLEngine sslEngine = context.createSSLEngine();
                    sslEngine.setUseClientMode(true); //客户端端模式
                    ch.pipeline().addLast("ssl", new SslHandler(sslEngine));
                    ch.pipeline().addLast(new RemoteTCPHandler());
                    ch.pipeline().addLast(new HttpResponseDecoder());
                    ch.pipeline().addLast(new HttpObjectAggregator(maxContentLength));
                    ch.pipeline().addLast(new HttpSendHandler());
                }
            });
        }
    }

    public static void main(String[] args)throws  Exception{


        //加载日志
        LogBackConfigLoader.load();

        try {
            new ProxyServer().start();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
